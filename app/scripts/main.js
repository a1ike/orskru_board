$(document).ready(function () {

  $('#or-mob-nav-toggle').on('click', function (e) {
    e.preventDefault();
    $('.or-form__header').slideToggle('fast');
    $('.or-filter').slideToggle('fast');
  });

  $('.or-item__big').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    dots: false,
    asNavFor: '.or-item__small'
  });
  $('.or-item__small').slick({
    slidesToShow: 7,
    slidesToScroll: 1,
    asNavFor: '.or-item__big',
    arrows: false,
    dots: false,
    centerMode: false,
    focusOnSelect: true
  });
});
